    </section>
    <footer id="page_footer">
        <div class="container">
            <div class="row bottom_line">
                <div class="col-sm-4 bottom_line-item_wrapp">
                    <a href="tel:380382756576" class="bottom_line-item"><strong>tel:</strong> +38 (0382) 75-65-76</a>
                </div>
                <div class="col-sm-4 bottom_line-item_wrapp">
                    <a href="tel:380677065675" class="bottom_line-item"><strong>mob:</strong> (067) 706-56-75</a>
                </div>
                <div class="col-sm-4 bottom_line-item_wrapp">
                     <a href="mailto:<?= $email ?>" class="bottom_line-item"><strong>e-mail:</strong> <?= $email ?></a>
                </div>
            </div>
            
        </div>
        <div class="copyright_line">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 copyright_line-copyright">
                        Copyright © 2017 ItalianUPS
                    </div>
                    <div class="col-md-4 text-center">
                        <?= $open ?>
                    </div>
                    <div class="col-sm-4 copyright_line-studio_link_wrapp">
                        <a href="http://web-systems.solutions/" target="_blank" class="copyright_line-studio_link"><i class="icon-ws hidden-xs">&nbsp;</i>создание сайта web-systems.solutions</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <button class="scrolltop-btn"><i class="icon-arrow-right"></i></button>

        <div class="modal fade" id="quick_order" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              
            </div>
          </div>
        </div>
      

                <div id="callback" class="callback">
                  <div class="callback-header">
                      <h3 class="callback-header_title"><?php echo $text_call; ?></h3>
                  </div>
                  <div class="callback-body">
                      <form class="form-horizontal request_call-form" id="request_call-form">
                          <div class="form-group required">
                              <div class="col-xs-12">
                                  <input type="text" name="name" placeholder="<?php echo $entry_name; ?>" class="form-control"/>
                              </div>
                          </div>
                          <div class="form-group required">
                              <div class="col-xs-12">
                                  <input type="text" name="phone" placeholder="<?php echo $entry_phone; ?>" class="form-control"/>
                              </div>
                          </div>
                          <div class="form-group required">
                              <div class="col-xs-12">
                                  <textarea name="message" rows="5" placeholder="<?php echo $entry_message; ?>" class="form-control"></textarea>
                              </div>
                          </div>
                          <?php if (isset($site_key) && $site_key) { ?>
                          <div class="form-group">
                              <div class="col-sm-12">
                                  <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                              </div>
                          </div>
                          <?php } elseif(isset($captcha) && $captcha){ ?>
                          <?php echo $captcha; ?>
                          <?php } ?>
                          <input name="goal" value="callback_request" type="hidden">
                          <div class="buttons">
                              <button type="button" id="request_call-btn" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><span><?php echo $button_continue; ?></span><i class="icon-double_angle_right"></i></button>
                          </div>
                      </form>
                  </div>
                </div>
                <button class="callback-btn" type="button"><i class="icon-phone"></i><i class="icon-close"></i></button>
                <script type="text/javascript">
                $('.callback-btn').on('click', function () {
                    $('#callback').toggleClass('visible');
                    $(this).toggleClass('visible');
                });
                $('#request_call-btn').on('click', function () {
                    $.ajax({
                        url: 'index.php?route=sendmess/send_message/send',
                        type: 'post',
                        dataType: 'json',
                        data:  $("#request_call-form").serialize(),
                        beforeSend: function () {
                            if ($("textarea").is("#g-recaptcha-response")) {
                                grecaptcha.reset();
                            }
                            $('#request_call-btn').button('loading');
                        },
                        complete: function () {
                            $('#request_call-btn').button('reset');
                        },
                        success: function (json) {
                            if (json['error']) {
                                $('.error').remove();
                                if (json['error_name']){
                                  $('#request_call-form input[name="name"]').after('<label class="error">' + json['error_name'] + '</label>');
                                  //$('#request_call-form input[name="name"]').parent().parent('.form-group').addClass('has-error');
                                }
                                if (json['error_phone']){
                                  $('#request_call-form input[name="phone"]').after('<label class="error">' + json['error_phone'] + '</label>');
                                  //$('#request_call-form input[name="phone"]').parent().parent('.form-group').addClass('has-error');
                                }
                            }
                            if (json['success']) {
                                $('#request_call-form .error').remove();
                                $('input[name=\'name\']').val('');
                                $('input[name=\'phone\']').val('');

                                $('#request_call-form').html(json['success']);
                                $('.user_message_wrapp').animate({
                                  opacity: 1
                                }, 300);
                            }
                        }
                    });
                });
                </script>
			
</body>
</html>