<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
    <base href="<?php echo $base; ?>" />
<?php foreach($extra_tags as $extra_tag) {?>
<meta <?php echo ($extra_tag['name']) ? 'name="' . $extra_tag['name'] . '" ' : ''; ?><?php echo (!in_array($extra_tag['property'], array("noprop", "noprop1", "noprop2", "noprop3", "noprop4"))) ? 'property="' . $extra_tag['property'] . '" ' : ''; ?> content="<?php echo addslashes($extra_tag['content']); ?>" />
<?php } ?>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $og_url; ?>" />
    <?php if ($og_image) { ?>
    <meta property="og:image" content="<?php echo $og_image; ?>" />
    <?php } else { ?>
    <meta property="og:image" content="<?php echo $logo; ?>" />
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>" />
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/slick-theme.css">
    <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/slick.css">
    <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/icomoon.css">
    <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/jquery-ui.structure.css">
    <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/general.css">
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
    <script src="catalog/view/theme/default/js/jquery-ui.min.js"></script>
    <script src="catalog/view/theme/default/js/jquery.mask.min.js"></script>
<script src="catalog/view/javascript/mf/jquery-ui.min.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <script src="catalog/view/theme/default/js/slick.min.js" type="text/javascript"></script>
    <script src="catalog/view/theme/default/js/general.js" type="text/javascript"></script>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>

      <?php if ( isset($popup_purchase_data['status']) && $popup_purchase_data['status'] ) { ?>
        <script src="catalog/view/javascript/popup_purchase/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <link href="catalog/view/javascript/popup_purchase/magnific-popup.css" rel="stylesheet" media="screen" />
        <link href="catalog/view/theme/default/stylesheet/popup_purchase/stylesheet.css" rel="stylesheet" media="screen" />
        <script type="text/javascript">
        $(function() {
          <?php if ($popup_purchase_data['allow_page']) { ?>
          $.each($("[onclick^='cart.add']"), function() {
            var product_id = $(this).attr('onclick').match(/[0-9]+/);
            $(this).parent().before("<div class='button-group popup-purchase-button'><button><?php echo $popup_purchase_text['button_purchase_now']; ?></button></div>").prev().attr('onclick', 'get_popup_purchase(\'' + product_id + '\');');
          });
          $.each($(".product-thumb [onclick^='get_popup_cart']"), function() {
            var product_id = $(this).attr('onclick').match(/[0-9]+/);
            $(this).parent().before("<div class='button-group popup-purchase-button'><button><?php echo $popup_purchase_text['button_purchase_now']; ?></button></div>").prev().attr('onclick', 'get_popup_purchase(\'' + product_id + '\');');
          });
          <?php } ?>
          var main_product_id = $('input[name=\'product_id\']').val();
          $('#button-cart').parent().parent().after( "<button class='btn btn-primary'><span><?php echo $popup_purchase_text['button_purchase_now']; ?></span><i class='icon-double_angle_right'></i></button>" ).next().attr('onclick', 'get_popup_purchase(\'' + main_product_id + '\');');
        });
        function get_popup_purchase(product_id) {  
          //$.magnificPopup.open({
            //tLoading: '<img src="catalog/view/theme/default/stylesheet/popup_purchase/ring-alt.svg" />',
            //items: {
              //src: 'index.php?route=module/popup_purchase&product_id='+product_id,
              //type: 'ajax'
            //}
          //});
          $('#quick_order').find('.modal-content').load('index.php?route=module/popup_purchase&product_id=' + product_id);
          $('#quick_order').modal('show');
        }
        </script>
      <?php } ?>
      
</head>
<body class="<?php echo $class; ?>">
    <section id="top_line" class="hidden-xs hidden-sm">
        <div class="container">
            <div class="row top_line">
                <div class="col-md-4 top_line-item_wrapp">
                    <div class="top_line-item"><?= $text_feature_1; ?></div>
                </div>
                <div class="col-md-4 top_line-item_wrapp">
                    <div class="top_line-item"><?= $text_feature_2; ?></div>
                </div>
                <div class="col-md-4 top_line-item_wrapp">
                    <div class="top_line-item"><?= $text_feature_3; ?></div>
                </div>
            </div>
        </div>
    </section>
    <header id="page_header">
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-xs-4">
                    <?php if ($logo) { ?>
                        <?php if ($home == $og_url) { ?>
                          <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" class="logo-img" alt="<?php echo $name; ?>" class="img-responsive" />
                        <?php } else { ?>
                          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" class="logo-img" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="col-md-8 col-sm-10 col-xs-8">
                    <nav class="main_menu-wrapp">
                        <button type="button" class="main_menu-btn navbar-toggle collapsed">
                            <span></span>
                        </button>
                        <ul class="main_menu">
                    
                            <?php if ($categories): ?>
                            <li class="main_menu-item <?= (strripos($class, 'category') != false) ? 'active' : '' ?>">
                                <a href="#" class="main_menu-item_inside" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Продукция</a>
                                <ul class="dropdown-menu main_menu-item_dropdown">
                                    <?php foreach ($categories as $category): ?>
                                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php foreach ($informations as $information): ?>
                            <li class="main_menu-item <?= ($information['href'] == $og_url) ? 'active' : '' ?>"><a href="<?php echo $information['href'] ?>" class="main_menu-item_inside"><?php echo $information['title'] ?></a></li>
                            <?php endforeach; ?>
                            <li class="main_menu-item <?= (strripos($class, 'news') != false) ? 'active' : '' ?>"><a href="<?php echo $articles; ?>" class="main_menu-item_inside"><?php echo $text_articles; ?></a></li>
                            <li class="main_menu-item <?= (strripos($class, 'contact') != false) ? 'active' : '' ?>"><a href="<?php echo $contact; ?>" class="main_menu-item_inside"><?php echo $text_contact; ?></a></li>  
                        </ul>
                    </nav>
                </div>
                <div class="col-md-1 hidden-xs user_menu-wrapp">
                    <div class="user_menu">
                        <?= $cart; ?>
                        <?php if ($logged): ?>
                        <a href="<?= $account ?>" class="icon-profile user_menu-item active"></a>
                        <?php else: ?>
                        <a href="#" class="icon-profile user_menu-item quick_authorization"></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-1 hidden-xs language_switcher-wrapp">
                    <?php echo $language; ?>
                </div>
            </div>
        </div>
    </header>

                <?php
                    if (!$logged)
                        echo $quicksignup;
                ?>
            
    <section id="main_wrapp">
