<?php
// Heading
$_['heading_title']  = 'Зв’язатися з нами';

// Text
$_['text_location']  = 'Наша Адреса';
$_['contact_form_title']  = 'Форма зворотнього зв\'язку';
$_['text_store']     = 'Наші магазини';
$_['text_contact']   = 'Форма зв’язку';
$_['text_address']   = 'Адреса';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Час роботи';
$_['text_comment']   = 'Коментар';
$_['text_success']   = '<p>Ваш запит був успішно відправлений адміністрації магазину!</p>';

// Entry
$_['entry_firstname']= 'Ім’я';
$_['entry_lastname'] = 'Прізвище';
$_['entry_phone']     = 'Телефон';
$_['entry_email']    = 'E-Mail для зв’язку';
$_['entry_enquiry']  = 'Повідомлення';

// Email
$_['email_subject']  = 'Повідомлення %s';

// Errors
$_['error_name']     = 'Ім’я має бути від 3 до 32 символів!';
$_['error_email']    = 'E-Mail вказано некоректно!';
$_['error_enquiry']  = 'Повідомлення має бути від 10 до 3000 символів!';
