<?php
// Text
$_['text_home']          = 'Главная';
$_['text_wishlist']      = 'Мои Закладки (%s)';
$_['text_shopping_cart'] = 'Корзина покупок';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'История платежей';
$_['text_download']      = 'Файлы для скачивания';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Показать все';
$_['text_page']          = 'страница';
$_['text_articles']      = 'Статьи';
$_['text_contact']       = 'Контакты';
$_['text_feature_1']     = '<i class="icon-settings top_line-item_icon"></i>Более 30 лет опыта в производстве ИБП';
$_['text_feature_2']     = '<i class="icon-feature top_line-item_icon"></i>Официальная гарантия 2 года';
$_['text_feature_3']     = '<i class="icon-novaposhta top_line-item_icon"></i>Бесплатная доставка ИБП по адресу';

