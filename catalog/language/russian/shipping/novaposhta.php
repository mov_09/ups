<?php
/**
 * @category   OpenCart
 * @package    OCU OCU Nova Poshta
 * @copyright  Copyright (c) 2011 Eugene Lifescale (a.k.a. Shaman)
 * @modify     Upgrade up to OpenCart 2.0.x with NovaPoshta API v2.0 by Alex Tymchenko
 * @license    http://www.gnu.org/copyleft/gpl.html     GNU General Public License, Version 3
 */

// Text
$_['text_title']                = 'Новая Почта';
$_['text_novaposhta_warehouse'] = 'На отделение Новой Почты';
$_['text_novaposhta_express']   = 'Курьером Новой Почты';
$_['text_novaposhta_free']      = 'Новой Почтой ';
$_['text_city_error']           = 'Ваш адрес доставки не найден в базе данных Новой Почты. Проверьте правильность заполнения формы адреса или <a href="%s">свяжитесь с администрацией</a>!';
