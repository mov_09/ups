$(document).ready(function(){
//	$('.common-home #column-left').removeClass('hidden-xs col-sm-3').addClass('col-sm-7');
//	$('.common-home #column-right').removeClass('hidden-xs col-sm-3').addClass('col-sm-5');

	$('.simplecheckout-left-column').append($('#buttons'));
//
	$('input[type="radio"], input[type="checkbox"]').checkboxradio();
//
//	$('.zoo-item').ZooMove();
//
	$('input[name="phone"], input[name="telephone"]').mask('+00 (000) 000-00-00');

    $.fn.wrapStart = function (numWords) { 
        var node = this.contents().filter(function () { 
                return this.nodeType == 3 
            }).first(),
            text = node.text(),
            first = text.split(" ", numWords).join(" ");

        if (!node.length)
            return;

        node[0].nodeValue = text.slice(first.length);
        node.before('<span>' + first + '</span><br/>');
    };
    $('.category-title').each(function(){
        $(this).wrapStart(1);
    });
    
    $('.scrolltop-btn').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 600);
        return false;
    });
    
    if ($('body').hasClass('account-simpleedit'))
        $('.simpleedit_link').parent().addClass('active');
    if ($('body').hasClass('account-password'))
        $('.password_link').parent().addClass('active');
    if ($('body').hasClass('account-order'))
        $('.order_link').parent().addClass('active');
    if ($('body').hasClass('account-address'))
        $('.address_link').parent().addClass('active');
    
    $('.main_menu-btn').click(function(){
        $('.main_menu').toggleClass('open');
        $(this).toggleClass('active');
        $('.scrolltop-btn').toggleClass('hidden');
    });
    
    $('.main_slider-slide_triangle').css('border-left-width', $('.main_slider').innerWidth());
    
    $('.form-control').focus(function(){
        $(this).next('.error').remove();
    })
        
});

$(window).resize(function(){
    $('.main_slider-slide_triangle').css('border-left-width', $('.main_slider').innerWidth());
});

$(window).load(function(){
    
});

$(window).scroll(function (){
    if ($(this).scrollTop() > 250) {
        $('.scrolltop-btn').fadeIn();
    } else {
        $('.scrolltop-btn').fadeOut();
    }
});