<?php echo $header; ?>
<div class="breadcrumb-wrapp">
    <div class="container">
        <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <?php if($i+1<count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
              <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1 class="section-title"><?php echo $heading_title; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group required">
                    <div class="col-md-12">
                      <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                      <?php if ($error_password) { ?>
                      <div class="text-danger"><?php echo $error_password; ?></div>
                      <?php } ?>
                    </div>
                </div>
                <div class="form-group required">
                    <div class="col-md-12">
                      <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
                      <?php if ($error_confirm) { ?>
                      <div class="text-danger"><?php echo $error_confirm; ?></div>
                      <?php } ?>
                    </div>
                </div>
                <div class="buttons clearfix">
                  <div class="pull-right">
                      <button type="submit" class="btn btn-primary"><?php echo $button_continue; ?></button>
                  </div>
                </div>
            </div>
        </div>  
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>