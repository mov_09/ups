<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<div class="review">
    <div class="review-author">
        <?php echo $review['author']; ?>
    </div>
    <div class="review-text">
        <?php echo $review['text']; ?>
    </div>
    <time class="review-date"><?php echo $review['date_added']; ?></time>
</div>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
