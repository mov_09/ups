<?php echo $header; ?>
<div class="breadcrumb-wrapp">
    <div class="container">
        <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <?php if($i+1<count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
              <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="container">
  <h1 class="section-title"><?php echo $heading_title; ?></h1>
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if ($products) { ?>
      <div class="product-list">
        <?php foreach ($products as $product) { ?>
        <div class="product">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                </div>
                <div class="col-md-6 col-sm-8">
                    <h4 class="product-title"><?php echo $product['name']; ?></h4>
                    <div class="product-short_desc"><?php echo $product['short_desc']; ?></div>
                    <ul class="product-attributes">
                    <?php
                        foreach ($product['attribute_groups'] as $attribute_group):
                            foreach ($attribute_group['attribute'] as $attribute):
                    ?>
                        <li><?php echo $attribute['text']; ?></li>
                    <?php 
                          endforeach;
                      endforeach;
                    ?>
                    </ul>
                    <div class="visible-sm">
                        <div class="buttons">
                            <a href="<?php echo $product['href']; ?>" class="btn btn-primary"><span>Подробнее</span><i class="icon-double_angle_right"></i></a>
                        </div>
                        <?php if (!$info_cat): ?>
                        <div class="product-stock_status"><?php echo $product['stock']; ?></div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="clearfix visible-sm"></div>
                <div class="col-md-3 hidden-sm product-btns_wrapp">
                    <div class="buttons">
                        <a href="<?php echo $product['href']; ?>" class="btn btn-primary btn-block"><span>Подробнее</span><i class="icon-double_angle_right"></i></a>
                    </div>
                    <?php if (!$info_cat): ?>
                    <div class="product-stock_status"><?php echo $product['stock']; ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <?php if ($product['new'] == 1): ?>
            <div class="product-label new">
                Новинка
            </div>
            <?php 
                else:
                if ($product['special']):
            ?>
            <div class="product-label special">
                Акция
            </div>
            <?php
                endif; 
                endif;
            ?>
        </div>
        <?php } ?>
      </div>
      <div class="text-center"><?php echo $pagination; ?></div>
      <?php if ($description) { ?>
      <?php echo $description; ?>
      <?php } ?>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>
<?php echo $footer; ?>
