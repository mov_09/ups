<div class="modal fade authorization_modal" id="modal-authorization" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
    		<div class="modal-header clearfix">
    			<h3 class="modal-title"><?= $text_signin_register ?></h3>
    			<button type="button" class="icon-close modal-close_btn" data-dismiss="modal" aria-hidden="true"></button>
    		</div>
    		<div class="modal-body">
	    		<div class="row">
					<div class="col-sm-6 authorization_modal-login" id="quick-login">
						<h4 class="authorization_modal-title"><?php echo $text_returning; ?></h4>
						<div class="form-group required">
							<input type="text" name="email" value="" class="form-control" placeholder="<?php echo $entry_email; ?>">
						</div>
						<div class="form-group required">
							<input type="password" name="password" value="" class="form-control" placeholder="<?php echo $entry_password; ?>">
						</div>
						<div class="buttons text-right">
                            <button type="button" class="btn btn-primary loginaccount"  data-loading-text="<?php echo $text_loading; ?>"><span><?php echo $text_login ?></span></button>
                            <br><br>
							<a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
						</div>
                        <?php //echo $modal_social; ?>
					</div>
					
					<div class="col-sm-6 authorization_modal-register" id="quick-register">
						<h4 class="authorization_modal-title"><?php echo $text_new_customer; ?></h4>
						<div class="form-group required">
							<input type="text" name="name" value="" class="form-control" placeholder="<?php echo $entry_name; ?>">
						</div>
						<div class="form-group required">
							<input type="text" name="email" value="" class="form-control" placeholder="<?php echo $entry_email; ?>">
						</div>
						<div class="form-group required">
							<input type="text" name="telephone" value="" class="form-control" placeholder="<?php echo $entry_telephone; ?>">
						</div>
						<div class="form-group required">
							<input type="password" name="password" value="" class="form-control" placeholder="<?php echo $entry_password; ?>">
						</div>
						<div class="buttons text-right">
						<?php if ($text_agree) { ?>
							<input type="checkbox" name="agree" value="1" />&nbsp;<?php echo $text_agree; ?>
							<br><br>
							<button type="button" class="btn btn-primary createaccount"  data-loading-text="<?php echo $text_loading; ?>" ><span><?php echo $text_register; ?></span></button>
						<?php }else{ ?>
							<button type="button" class="btn btn-primary createaccount" data-loading-text="<?php echo $text_loading; ?>" ><span><?php echo $text_register; ?></span></button>
						<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript"><!--
$(document).delegate('.quick_authorization', 'click', function(e) {
	e.preventDefault();
	$('#modal-authorization').modal('show');
});
//--></script>
<script type="text/javascript"><!--
$('#quick-register input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-register .createaccount').trigger('click');
	}
});
$('#quick-register .createaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/register',
		type: 'post',
		data: $('#quick-register input[type=\'text\'], #quick-register input[type=\'password\'], #quick-register input[type=\'checkbox\']:checked'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-register .createaccount').button('loading');
			$('#quick-register .alert-danger').remove();
		},
		complete: function() {
			$('#quick-register .createaccount').button('reset');
		},
		success: function(json) {
			$('#modal-register .form-group').removeClass('has-error');
			
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}
			if (json['error_name']) {
				$('#quick-register input[name="name"]').parent().addClass('has-error');
				$('#quick-register input[name="name"]').focus();
			}
			if (json['error_email']) {
				$('#quick-register input[name="email"]').parent().addClass('has-error');
				$('#quick-register input[name="email"]').focus();
			}
			if (json['error_telephone']) {
				$('#quick-register input[name="telephone"]').parent().addClass('has-error');
				$('#quick-register input[name="telephone"]').focus();
			}
			if (json['error_password']) {
				$('#quick-register input[name="password"]').parent().addClass('has-error');
				$('#quick-register input[name="password"]').focus();
			}
            if (json['error_repassword']) {
				$('#quick-register input[name="repassword"]').parent().addClass('has-error');
				$('#quick-register input[name="repassword"]').focus();
			}
			if (json['error']) {
				$('#quick-register .modal-title').after('<div class="alert alert-danger" style="margin: 0 0 20px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['now_login']) {
				$('.quick-login').before('<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li></ul></li>');
				
				$('.quick-login').remove();
			}
			if (json['success']) {
				$('#modal-authorization .modal-title').html(json['heading_title']);
				success = json['text_message'];
				$('#modal-authorization .modal-body').addClass('message').html(success);
                $('#modal-authorization').on('hidden.bs.modal', function () {
                   window.location.href="index.php?route=account/account";
                });
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#quick-login input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-login .loginaccount').trigger('click');
	}
});
$('#quick-login .loginaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/login',
		type: 'post',
		data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-login .loginaccount').button('loading');
			$('#quick-login .alert-danger').remove();
		},
		complete: function() {
			$('#quick-login .loginaccount').button('reset');
		},
		success: function(json) {
			$('#modal-login .form-group').removeClass('has-error');
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}
			
			if (json['error']) {
				$('#quick-login .modal-title').first().after('<div class="alert alert-danger" style="margin: 0 0 20px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
				$('#quick-login input[name="email"]').parent().addClass('has-error');
				$('#quick-login input[name="password"]').parent().addClass('has-error');
				$('#quick-login input[name="email"]').focus();
			}
			if(json['success']){
				loacation();
				$('#modal-login').modal('hide');
			}
			
		}
	});
});
//--></script>
<script type="text/javascript"><!--
function loacation() {
	location.reload();
}
//--></script>