<?php if (count($languages) > 1): ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language" class="language_switcher">
    <?php foreach ($languages as $language): ?>
    <a href="<?php echo $language['code']; ?>" class="language_switcher-item <?php if ($language['code'] == $code): echo 'active'; endif; ?>"><?php echo $language['code']; ?></a>
    <?php endforeach; ?>
    <input type="hidden" name="code" value="" />
    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
<?php endif; ?>

