<a href="<?php echo $cart; ?>" class="icon-shoppingcart user_menu-item"></a>
<ul class="dropdown-menu cart_dropdown">
    <?php if ($products || $vouchers) { ?>
    <li>
      <table class="table table-striped cart_dropdown-products_table">
        <?php foreach ($products as $product) { ?>
        <tr>
          <td class="text-center"><?php if ($product['thumb']) { ?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"></a>
            <?php } ?></td>
          <td class="text-left">
            <a href="<?php echo $product['href']; ?>" class="cart_dropdown-products_table_title"><?php echo $product['name']; ?></a>
            <?php if ($product['option']) { ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
            <?php } ?>
            <?php } ?>
            <?php if ($product['recurring']) { ?>
            <br />
            - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
            <?php } ?></td>
          <td class="text-right">x&nbsp;<?php echo $product['quantity']; ?></td>
          <td class="text-right cart_dropdown-products_table_price"><?php echo $product['total']; ?></td>
          <td class="text-center"><button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="cart_dropdown-products_table_btn btn-xs"><i class="icon-close"></i></button></td>
        </tr>
        <?php } ?>
      </table>
    </li>
    <?php } else { ?>
    <li class="text-center">
      <?php echo $text_empty; ?>
    </li>
    <?php } ?>
</ul>
<input type="hidden" value="<?php echo $checkout; ?>" name="checkout_link" id="checkout_link">