<section>
    <div class="container">
        <h2 class="section-title"><?php echo $heading_title; ?></h2>
        <div class="article_module row">
            <?php
                $counter = 0;
                foreach ($article as $articles):
                    $counter++;
                    if ($counter == 1):
            ?>
            <div class="col-sm-6">
                <div class="article_module-article main" style="background-image: url(<?php echo $articles['thumb']; ?>);">
                    <div>
                        <?php if ($articles['name']) { ?>
                            <div class="article_module-article_title"><?php echo $articles['name']; ?></div>
                        <?php } ?>
                        <?php if ($articles['description']) { ?>
                            <div class="article_module-article_short_desc"><?php echo $articles['description']; ?></div>
                        <?php } ?>
                        <?php if ($articles['button']) { ?>
                            <a class="btn btn-primary article_module-btn" href="<?php echo $articles['href']; ?>"><span><?php echo $button_more; ?></span><i class="icon-double_angle_right"></i></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
            <?php
                   else:
            ?>
            <div class="article_module-article">
                <?php if ($articles['name']) { ?>
                    <div class="article_module-article_title"><?php echo $articles['name']; ?></div>
                <?php } ?>
                <?php if ($articles['description']) { ?>
                    <div class="article_module-article_short_desc"><?php echo $articles['description']; ?></div>
                <?php } ?>
                <?php if ($articles['button']) { ?>
                    <div class="text-right">
                        <a class="article_module-article_rmb" href="<?php echo $articles['href']; ?>">[<?php echo $button_more; ?>]</a>
                    </div>
                <?php } ?>
            </div>
            <?php
                   endif;
               endforeach;
            ?>
            </div>
        </div>
        <div class="text-right">
            <a href="<?php echo $newslink; ?>" class="btn btn-default"><span><?php echo $text_headlines; ?></span><i class="icon-double_angle_right"></i></a>
        </div>
    </div>
</section>
