<section id="main_slider_<?php echo $module; ?>" class="main_slider">
    <?php foreach ($banners as $banner): ?>
    <div class="main_slider-slide">
        <div class="row">
            <div class="col-sm-6 col-md-offset-2 col-sm-offset-1 text-right">
                <img src="<?php echo $banner['image']; ?>" alt="Title" class="main_slider-slide_img">
            </div>
            <div class="col-md-4 col-sm-5">
                <div class="main_slider-slide_desc_wrapp">
                    <div class="main_slider-slide_triangle visible-xs"></div>
                    <div class="main_slider-slide_desc">
                        <div class="main_slider-slide_title">
                           <?php echo $banner['title']; ?>
                        </div>
                        <img src="catalog/view/theme/default/images/main_slider-logo.png" alt="<?php echo $banner['title']; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</section>
<div class="container main_slider-arrow_wrapp">
    <div class="main_slider-arrow">
        <i class="icon-angle-arrow-right main_slider-arrow_part"></i>
        <i class="icon-angle-arrow-right main_slider-arrow_part"></i>
        <i class="icon-angle-arrow-right main_slider-arrow_part"></i>
    </div>
</div>
<script>
    $('#main_slider_<?php echo $module; ?>').slick({
        slideToShow: 1,
        fade: true,
        autoplay: true
    });
</script>
