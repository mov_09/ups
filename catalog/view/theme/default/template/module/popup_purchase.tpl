<div class="modal-header">
    <h4 class="modal-title" id="myModalLabel"><?php echo $heading_title; ?></h4>
    <button type="button" class="icon-close modal-close_btn" data-dismiss="modal" aria-hidden="true"></button>
</div>
<div class="modal-body quick_order-body">
    <?php if ($stock_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $stock_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($minimum > 1) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <div class="row">
    <?php
        if ($popup_purchase_data['image']) {
            if ($thumb) {
                $class = 'col-md-7 col-md-offset-1';
    ?>
        <div class="col-md-4">
             <img src="<?php echo $thumb; ?>" class="quick_order-product_img" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
        </div>
    <?php 
            }
            else {
                $class = 'col-md-12';
            }
        }
    ?>
        <div class="<?= $class ?>">
          <div class="quick_order-product_title"><?php echo $product_name; ?></div>
          <?php if ($popup_purchase_data['description']) { ?>
          <div class="quick_order-description"><?php echo $description?></div>
          <?php } ?>
<!--
          <?php if ($price) { ?>
                <div class="quick_order-price_wrapp">
                    <?php if (!$special) { ?>
                    <div class="quick_order-price" id="main-price"><?php echo $price; ?></div>
                    <?php } else { ?>
                    <span class="quick_order-price" id="special-price"><?php echo $special; ?></span><span class="quick_order-old_price" id="main-price"><?php echo $price; ?></span>
                    <?php } ?>
                </div>
                <?php foreach ($discounts as $discount) { ?>
                <?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?>
                <?php } ?>
          <?php } ?>
-->
          <?php if ($attribute_groups) { ?>
          <ul class="quick_order-product_attributes">
            <?php
                foreach ($attribute_groups as $attribute_group):
                    foreach ($attribute_group['attribute'] as $attribute):
            ?>
                <li><?php echo $attribute['text']; ?></li>
            <?php 
                  endforeach;
              endforeach;
            ?>
          </ul>
          <?php } ?>
          <?php if ($popup_purchase_data['quantity']) { ?>
            <div class="payment-quantity">
                <label class="quick_order-product_quantity_label"><?php echo $entry_quantity; ?></label>
                <div class="quick_order-product_quantity">
                    <button class="quick_order-product_quantity_btn" type="button" onclick="$(this).next().val(~~$(this).next().val()-1); update_prices('<?php echo $product_id; ?>');">
                        <i class="fa fa-minus"></i>
                    </button>
                    <input class="quick_order-product_quantity_input" type="text" onchange="update_prices( '<?php echo $product_id; ?>' ); return validate(this);" onkeyup="update_prices( '<?php echo $product_id; ?>' ); return validate(this);" name="quantity" value="<?php echo $minimum; ?>">
                    <button class="quick_order-product_quantity_btn" type="button" onclick="$(this).prev().val(~~$(this).prev().val()+1); update_prices('<?php echo $product_id; ?>');">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <?php } else { ?>
            <input type="hidden" name="quantity" value="<?php echo $minimum; ?>" />
          <?php } ?>
        </div>
    </div>
    <?php if (!$stock_warning) { ?>
    <form method="post" enctype="multipart/form-data" id="purchase-form">
        <input name="product_id" value="<?php echo $product_id; ?>" style="display: none;" type="hidden" />
        <div class="row quick_order-customer_info">
            <div class="col-sm-6">
                <?php if ($popup_purchase_data['firstname']) { ?>
                <div class="form-group">
                    <input name="firstname" value="<?php echo $firstname;?>" class="form-control" placeholder="<?php echo $enter_firstname; ?>">
                </div>
                <?php } ?>
                <?php if ($popup_purchase_data['telephone']) { ?>
                <div class="form-group">
                    <input name="telephone" value="<?php echo $telephone;?>" class="form-control" placeholder="<?php echo $enter_telephone; ?>">
                </div>
                <?php } ?>
            </div>
            <div class="col-sm-6">
                <?php if ($popup_purchase_data['email']) { ?>
                <div class="form-group">
                    <input name="email" value="<?php echo $email;?>" class="form-control" placeholder="<?php echo $enter_email; ?>">
                </div>
                <?php } ?>
                <?php if ($popup_purchase_data['comment']) { ?>
                <div class="form-group">
                    <input name="comment" class="form-control" value="<?=  $comment; ?>" placeholder="<?php echo $enter_comment; ?>">
                </div>
                <?php } ?>
            </div>
        </div>
        <?php if ($options) { ?>
        <div class="info-heading-2"><?php echo $text_option; ?></div>
        <?php foreach ($options as $option) { ?>
        <?php if ($option['type'] == 'select') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" onchange="update_prices('<?php echo $product_id; ?>');">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
            </option>
            <?php } ?>
          </select>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'radio') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label"><?php echo $option['name']; ?></label>
          <div id="input-option<?php echo $option['product_option_id']; ?>">
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <div class="radio">
              <label>
                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" onchange="update_prices('<?php echo $product_id; ?>');"/>
                <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
              </label>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'checkbox') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label"><?php echo $option['name']; ?></label>
          <div id="input-option<?php echo $option['product_option_id']; ?>">
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" onchange="update_prices('<?php echo $product_id; ?>');"/>
                <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
              </label>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'image') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label"><?php echo $option['name']; ?></label>
          <div id="input-option<?php echo $option['product_option_id']; ?>">
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <div class="radio">
              <label>
                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" onchange="update_prices('<?php echo $product_id; ?>');"/>
                <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
              </label>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'text') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'textarea') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'file') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label"><?php echo $option['name']; ?></label>
          <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
          <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'date') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <div class="input-group date">
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
            </span></div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'datetime') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <div class="input-group datetime">
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            <span class="input-group-btn">
            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
            </span></div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'time') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <div class="input-group time">
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            <span class="input-group-btn">
            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
            </span></div>
        </div>
        <?php } ?>
        <?php } ?>
        <?php } ?>
    </form>
    <?php } ?>
    <?php if (!$stock_warning) { ?>
    <div class="row buttons">
        <div class="col-sm-6">
            
        </div>
        <div class="col-sm-6 text-right">
            <a id="popup-checkout-button" class="btn btn-primary" style="min-width: 190px;"><span><?php echo $button_checkout; ?></span><i class="icon-double_angle_right"></i></a>
        </div>
    </div>
    <?php } ?>
</div>

<?php if (!$stock_warning) { ?>
<script src="catalog/view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false,
});

$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});

$('.time').datetimepicker({
  pickDate: false,
});

$(document).on('click', 'button[id^=\'button-upload\']', function() {
  var node = this;

  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input').after('<label class="error">' + json['error'] + '</label>');
          }

          if (json['success']) {
            alert(json['success']);

            $(node).parent().find('input').attr('value', json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<script type="text/javascript"><!--
function masked(element, status) {
  if (status == true) {
    $('<div/>')
    .attr({ 'class':'masked' })
    .prependTo(element);
    $('<div class="masked_loading" />').insertAfter($('.masked'));
  } else {
    $('.masked').remove();
    $('.masked_loading').remove();
  }
}

<?php if ($popup_purchase_data['quantity']) { ?>
function validate( input ) {
  input.value = input.value.replace( /[^\d,]/g, '' );
}
<?php } ?>

$('#popup-checkout-button').on('click', function() {
  masked('#popup-purchase-wrapper', true);
  $.ajax({
      type: 'post',
      url:  'index.php?route=module/popup_purchase/make_order',
      dataType: 'json',
      data: $('#purchase-form').serialize(),
      success: function(json) {
        if (json['error']) {
          if (json['error']['field']) {
            masked('#popup-purchase-wrapper', false);
            $('.text-danger').remove();
            $.each(json['error']['field'], function(i, val) {
              $('[name="' + i + '"]').addClass('error_style').after('<label class="error">' + val + '</label>');
            });
          }
          if (json['error']['option']) {
            for (i in json['error']['option']) {
              masked('#popup-purchase-wrapper', false);
              $('.required .text-danger').remove();
              var element = $('#input-option' + i.replace('_', '-'));
              element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            }
          }
          if (json['error']['recurring']) {
            $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
          }
        } else {
          if (json['output']) {
            masked('#popup-purchase-wrapper', false);
            $('#popup-checkout-button').remove();
            $('#quick_order .modal-body').html(json['output']);

          }
        }
      }
  });
});

<?php if ($popup_purchase_data['quantity']) { ?>
function update_prices(product_id) {
  masked('#popup-purchase-wrapper', true);
  var input_val = $('#purchase-form').find('input[name=quantity]').val();
  var quantity = parseInt(input_val);

  <?php if ($minimum > 1) { ?>
    if (quantity < <?php echo $minimum; ?>) {
      quantity = $('#purchase-form').find('input[name=quantity]').val(<?php echo $minimum; ?>);
      masked('#popup-purchase-wrapper', false);
      return;
    }
  <?php } else { ?>
    if (quantity == 0) {
      quantity = $('#purchase-form').find('input[name=quantity]').val(1);
      masked('#popup-purchase-wrapper', false);
      return;
    }
  <?php } ?>

  $.ajax({
    url: 'index.php?route=module/popup_purchase/update_prices&product_id=' + product_id + '&quantity=' + quantity,
    type: 'post',
    dataType: 'json',
    data: $('#purchase-form').serialize(),
    success: function(json) {
      $('#main-price').html(json['price']);
      $('#special-price').html(json['special']);
      $('#main-tax').html(json['tax']);
      masked('#popup-purchase-wrapper', false);
    }
  });
}
<?php } ?>

$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
  $.ajax({
    url: 'index.php?route=product/product/getRecurringDescription',
    type: 'post',
    data: $('#purchase-form input[name=\'product_id\'], #purchase-form input[name=\'quantity\'], #purchase-form select[name=\'recurring_id\']'),
    dataType: 'json',
    beforeSend: function() {
      $('#recurring-description').html('');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();

      if (json['success']) {
        $('#recurring-description').html(json['success']);
      }
    }
  });
});
//--></script>
<?php } ?>
<style type="text/css">
<?php if ($popup_purchase_data['color_h1']) { ?>
#popup-purchase-wrapper .product-name {color:<?php echo $popup_purchase_data['color_h1']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['color_price']) { ?>
#popup-purchase-wrapper #main-price {color:<?php echo $popup_purchase_data['color_price']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['color_special_price']) { ?>
#popup-purchase-wrapper #special-price {color:<?php echo $popup_purchase_data['color_special_price']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['color_description']) { ?>
#popup-purchase-wrapper .product-description {color:<?php echo $popup_purchase_data['color_description']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['color_checkout_button']) { ?>
#popup-purchase-wrapper .popup-footer a {color:<?php echo $popup_purchase_data['color_checkout_button']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['color_close_button']) { ?>
#popup-purchase-wrapper .popup-footer button {color:<?php echo $popup_purchase_data['color_close_button']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['background_checkout_button']) { ?>
#popup-purchase-wrapper .popup-footer a {background:<?php echo $popup_purchase_data['background_checkout_button']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['background_close_button']) { ?>
#popup-purchase-wrapper .popup-footer button {background:<?php echo $popup_purchase_data['background_close_button']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['background_checkout_button_hover']) { ?>
#popup-purchase-wrapper .popup-footer a:hover {background:<?php echo $popup_purchase_data['background_checkout_button_hover']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['background_close_button_hover']) { ?>
#popup-purchase-wrapper .popup-footer button:hover {background:<?php echo $popup_purchase_data['background_close_button_hover']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['border_checkout_button']) { ?>
#popup-purchase-wrapper .popup-footer a {border-color:<?php echo $popup_purchase_data['border_checkout_button']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['border_close_button']) { ?>
#popup-purchase-wrapper .popup-footer button {border-color:<?php echo $popup_purchase_data['border_close_button']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['border_checkout_button_hover']) { ?>
#popup-purchase-wrapper .popup-footer a:hover {border-color:<?php echo $popup_purchase_data['border_checkout_button_hover']; ?>; }
<?php } ?>
<?php if ($popup_purchase_data['border_close_button_hover']) { ?>
#popup-purchase-wrapper .popup-footer button:hover {border-color:<?php echo $popup_purchase_data['border_close_button_hover']; ?>; }
<?php } ?>
</style>
