<section>
    <div class="container">
        <h2 class="section-title"><?= $text_our_features ?></h2>
        <div class="row">
            <?php
                foreach ($banners as $banner):
                    if ($banner['link']):
            ?>
                <a href="<?php echo $banner['link']; ?>" class="col-md-4 banner" style="background-image: url(<?php echo $banner['image']; ?>);">
                    <?php echo $banner['title']; ?>
                </a>
            <?php
                    else:       
            ?>
                <div class="col-sm-4 banner" style="background-image: url(<?php echo $banner['image']; ?>);">
                    <?php echo $banner['title']; ?>
                </div>
            <?php
                   endif;
                endforeach;
            ?>
        </div>
    </div>
</section>

