<section>
    <div class="container">
        <h2 class="section-title">Продукция</h2>
        <div class="row">
            <?php foreach ($categories as $category): ?>
            <figure href="<?php echo $category['href']; ?>" class="col-sm-6 category">
                <?php if ($category['thumb']): ?>
                <img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" class="category-img">
                <?php endif; ?>
                <figcaption class="category-short_desc">
                    <h4 class="category-title"><?php echo $category['name']; ?></h4>
                    <a href="<?php echo $category['href']; ?>" class="btn btn-primary">
                        <span>Перейти</span><i class="icon-double_angle_right"></i>
                    </a>
                </figcaption>
            </figure>
            <?php endforeach; ?>
        </div>
    </div>    
</section>
