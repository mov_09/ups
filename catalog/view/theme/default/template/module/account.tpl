<ul class="nav nav-tabs">
  <?php if (!$logged) { ?>
  <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
  <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
  <li><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></li>
  <?php } ?>
  <?php if ($logged) { ?>
  <li><a href="<?php echo $edit; ?>" class="simpleedit_link"><?php echo $text_edit; ?></a></li>
  <li><a href="<?php echo $password; ?>" class="password_link"><?php echo $text_password; ?></a></li>
  <?php } ?>
  <li><a href="<?php echo $address; ?>" class="address_link"><?php echo $text_address; ?></a></li>
  <li><a href="<?php echo $order; ?>" class="order_link"><?php echo $text_order; ?></a></li>
  <?php if ($logged) { ?>
  <li class="pull-right"><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
  <?php } ?>
</ul>
