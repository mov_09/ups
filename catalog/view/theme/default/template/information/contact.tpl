<?php echo $header; ?>
<div class="breadcrumb-wrapp">
    <div class="container">
        <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <?php if($i+1<count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
              <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="container">
  <div class="row">
   <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <h1 class="section-title"><?php echo $heading_title; ?></h1>
        <div class="row">
            <div class="col-md-4">
                <ul class="contact_info row">
                    <li class="col-md-12 col-sm-4 contact_info-item">
                        <i class="icon-mobile_phone contact_info-item_icon"></i>
                        <div class="contact_info-phones">
                            <?php
                                if ($phones):
                                    foreach($phones as $phone):    
                            ?>
                            <a href="tel:<?= preg_replace('#\D+#', '', $phone); ?>"><?= $phone; ?></a>
                            <?php
                                    endforeach;
                                endif;
                            ?>
                        </div>
                    </li>
                    <li class="col-md-12 col-sm-4 contact_info-item"><i class="icon-email contact_info-item_icon"></i><a href="mailto:<?= $store_email; ?>"><?= $store_email; ?></a></li>
                    <li class="col-md-12 col-sm-4 contact_info-item"><i class="icon-location contact_info-item_icon"></i><?= $address; ?><br><br><?= $open; ?></li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="contact_form-wrapp">
                    <h4 class="contact_form-title"><?= $contact_form_title ?></h4>
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal contact_form">
                      <div class="form-group required">
                        <div class="col-sm-6">
                          <input type="text" name="firstname" value="<?= $firstname ?>" placeholder="<?= $entry_firstname; ?>" class="form-control" />
                          <?php if ($error_firstname) { ?>
                          <label class="error"><?php echo $error_firstname; ?></label>
                          <?php } ?>
                        </div>
                        <div class="col-sm-6">
                          <input type="text" name="lastname" value="<?= $lastname ?>" placeholder="<?= $entry_lastname; ?>" class="form-control" />
                          <?php if ($error_lastname) { ?>
                          <label class="error"><?php echo $error_lastname; ?></label>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required">
                        <div class="col-sm-6">
                          <input type="text" name="phone" value="<?= $phone_num ?>" placeholder="<?= $entry_phone; ?>" class="form-control" />       
                        </div>
                        <div class="col-sm-6">
                          <input type="text" name="email" value="<?= $email ?>" placeholder="<?= $entry_email; ?>" class="form-control" />
                          <?php if ($error_email) { ?>
                          <label class="error"><?php echo $error_email; ?></label>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required">
                        <div class="col-sm-12">
                          <textarea name="enquiry" rows="4" id="input-enquiry" class="form-control" placeholder="<?= $entry_enquiry; ?>"><?= $enquiry ?></textarea>
                          <?php if ($error_enquiry) { ?>
                          <label class="error"><?php echo $error_enquiry; ?></label>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="buttons text-right">
                        <button class="btn btn-default" type="submit"><span><?= $button_submit; ?></span><i class="icon-double_angle_right"></i></button>
                      </div>
                    </form>
                </div>
            </div>
        </div>

        <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
        </div>
</div>
<?php echo $footer; ?>
