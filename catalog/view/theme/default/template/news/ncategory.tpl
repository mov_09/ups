
<?php if ($article) { ?>
	<div class="article-list">
		<?php foreach ($article as $articles) { ?>
		    <div class="article">
                <div class="row">
                    <?php
                        if ($articles['thumb']):
                            $class = 'col-md-9 col-sm-8';
                    ?>
                        <div class="col-md-3 col-sm-4 hidden-xs">
                            <img class="article-img" align="left" src="<?php echo $articles['thumb']; ?>" class="article-img" alt="<?php echo $articles['name']; ?>">
                        </div>
                    <?php
                        else:
                            $class = 'col-md-12';
                        endif;
                    ?>
                    <div class="<?php echo $class; ?>">
                        <?php if ($articles['name']) { ?>
                        <h4 class="article-title"><?php echo $articles['name']; ?></h4>
                        <?php } ?>
                        <div class="article-short_desc"><?php echo $articles['description']; ?></div>
                        <?php if ($articles['button']) { ?>
                        <a class="article-rmb" href="<?php echo $articles['href']; ?>">[<?php echo $button_more; ?>]</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
		<?php } ?>
  </div>
  <?php } ?>
  <div class="text-center">
        <?php echo $pagination; ?>
  </div>
	