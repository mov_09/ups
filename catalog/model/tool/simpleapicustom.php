<?php
/*
@author Dmitriy Kubarev
@link   http://www.simpleopencart.com
@link   http://www.opencart.com/index.php?route=extension/extension/info&extension_id=4811
*/

class ModelToolSimpleApiCustom extends Model {
    public function example($filterFieldValue) {
        $values = array();

        $values[] = array(
            'id'   => 'my_id',
            'text' => 'my_text'
        );

        return $values;
    }
    public function ShopAdress($filterFieldValue) {
        $values = array();

        if($filterFieldValue=='Хмельницкий') {
            $values[] = array(
                'id' => '1',
                'text' => 'г. Хмельницкий, ул. Курчатова, 2/1б'
            );
            $values[] = array(
                'id' => '2',
                'text' => 'г. Хмельницкий, ул. Проскуровская, 81'
            );
        } elseif ($filterFieldValue=='Городок') {
            $values[] = array(
                'id' => '1',
                'text' => ' ул. Грушевского, 84/2'
            );

        } else {
            $values[] = array(
                'id' => '1',
                'text' => 'Здесь магазина нет'
            );

        }

        return $values;
    }

    public function checkCaptcha($value, $filter) {
        if (isset($this->session->data['captcha']) && $this->session->data['captcha'] != $value) {
            return false;
        }

        return true;
    }

    public function getYesNo($filter = '') {
        return array(
            array(
                'id'   => '1',
                'text' => $this->language->get('text_yes')
            ),
            array(
                'id'   => '0',
                'text' => $this->language->get('text_no')
            )
        );
    }
    public function getNovapostOffices($city = '')
    {
        $offices = array();
        $this->load->model('extension/extension');
        if (isset($this->session->data['simple']['shipping_address'])) {
            $address = $this->session->data['simple']['shipping_address'];
            $results = $this->model_extension_extension->getExtensions('shipping');
            
            $this->load->model('shipping/novaposhta');
            $quote = $this->{'model_shipping_novaposhta'}->getQuote($address);
                        
            if (isset($quote['offices']['data'])) {
                foreach ($quote['offices']['data'] as $key => $value) {
                    $offices[] = array(
                        'id' => $key,
                        'text' => $value['Description']
                    );
                }
            }
        }
        if ($this->session->data['simple']['shipping_address']['city'] == ''):
            $offices = array();
            $offices[] = array(
                'id' => 'Не выбрано',
                'text' => 'Введите город доставки'
            );
        endif;
        return $offices;
    }
    public function getNovapostCities(){
        if (isset($this->request->get['filter'])) {
            $filter = $this->request->get['filter'];
        } else {
            $filter = '';
        }
        $city_response = $this->getResponse(
            $this->getRequest('Address', 'getCities', array('FindByString' => $filter))
        );
        if($city_response['data']){
            $rows = array();
            foreach ($city_response['data'] as $city){
                $rows[] = $city['DescriptionRu'];
            }
            return $rows;
        }
        else{
            return $filter;
        }
        
    }
    private function getRequest($modelName, $calledMethod, $methodProperties) {
        $request = array(
            'modelName' => $modelName,
            'calledMethod' => $calledMethod,
            'methodProperties' => $methodProperties,
            'apiKey' => $this->config->get('novaposhta_api_key')
        );

        return json_encode($request);
    }
    private function getResponse($request) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.novaposhta.ua/v2.0/json/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($response, true);
        if ($json) {
            return $json;
        }
        return $response;
    }

}
