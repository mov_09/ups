<?php
//XML
$_['signin_or_register']           = 'Sign In\Register';

// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Мої Закладки (%s)';
$_['text_shopping_cart'] = 'Кошик замовлень';
$_['text_category']      = 'Категорії';
$_['text_account']       = 'Особистий кабінет';
$_['text_register']      = 'Реєстрація';
$_['text_login']         = 'Авторизація';
$_['text_order']         = 'Історія замовлень';
$_['text_transaction']   = 'Історія платежів';
$_['text_download']      = 'Файли для скачування';
$_['text_logout']        = 'Вихід';
$_['text_checkout']      = 'Оформлення замовлення';
$_['text_search']        = 'Пошук';
$_['text_all']           = 'Показати всі';
$_['text_page']          = 'Сторінка';
$_['text_articles']      = 'Статті';
$_['text_contact']       = 'Контакти';
$_['text_feature_1']     = '<i class="icon-settings top_line-item_icon"></i>Більше 30 років досвіду у виробництві ДБЖ';
$_['text_feature_2']     = '<i class="icon-feature top_line-item_icon"></i>Офіційна гарантія 2 роки';
$_['text_feature_3']     = '<i class="icon-novaposhta top_line-item_icon"></i>Безкоштовна доставка ДБЖ за адресою';
