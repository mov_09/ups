    </section>
    <footer id="page_footer">
        <div class="container">
            <div class="row bottom_line">
                <div class="col-sm-4 bottom_line-item_wrapp">
                    <a href="tel:380382756576" class="bottom_line-item"><strong>tel:</strong> +38 (0382) 75-65-76</a>
                </div>
                <div class="col-sm-4 bottom_line-item_wrapp">
                    <a href="tel:380677065675" class="bottom_line-item"><strong>mob:</strong> (067) 706-56-75</a>
                </div>
                <div class="col-sm-4 bottom_line-item_wrapp">
                     <a href="mailto:<?= $email ?>" class="bottom_line-item"><strong>e-mail:</strong> <?= $email ?></a>
                </div>
            </div>
            
        </div>
        <div class="copyright_line">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 copyright_line-copyright">
                        Copyright © 2017 ItalianUPS
                    </div>
                    <div class="col-md-4 text-center">
                        <?= $open ?>
                    </div>
                    <div class="col-sm-4 copyright_line-studio_link_wrapp">
                        <a href="http://web-systems.solutions/" target="_blank" class="copyright_line-studio_link"><i class="icon-ws hidden-xs">&nbsp;</i>создание сайта web-systems.solutions</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <button class="scrolltop-btn"><i class="icon-arrow-right"></i></button>

        <div class="modal fade" id="quick_order" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              
            </div>
          </div>
        </div>
      
</body>
</html>