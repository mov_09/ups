<?php if (count($languages) > 1): ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language" class="language_switcher">
    <?php foreach ($languages as $language): ?>
    <a href="<?php echo $language['code']; ?>" class="language_switcher-item <?php if ($language['code'] == $code): echo 'active'; endif; ?>"><?php echo $language['code']; ?></a>
    <?php endforeach; ?>
    <input type="hidden" name="code" value="" />
      <input type="hidden" name="redirect_route" value="<?php echo $redirect_route; ?>" />
  <input type="hidden" name="redirect_query" value="<?php echo isset($redirect_query) ? $redirect_query : ''; ?>" />
  <input type="hidden" name="redirect_ssl" value="<?php echo isset($redirect_ssl) ? $redirect_ssl : ''; ?>" />
</form>
<?php endif; ?>

